'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const ArrayVerify = require('./models/arrayVerify');


const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

	let intArray = Array.from({length: 10}, () => Math.floor(Math.random() * 999) + 1);	

    let arrayVerify = new ArrayVerify();

    let response = arrayVerify.verify(intArray);

app.get('/', function (req, res) {
  res.send(response);
});

app.listen(3000, () => {
	console.log(`API REST corriendo en http://localhost:${port}`)
})