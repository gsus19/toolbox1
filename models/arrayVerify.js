'use strict'

function ArrayVerify(){};	

let strArray = [];	

ArrayVerify.prototype.verify = (intArray) => {
    intArray.forEach(num => {
		let divisibleBy3 = (num%3 == 0)? true : false;
		let divisibleBy5 = (num%5 == 0)? true : false;
		let include3 = (num.toString().includes('3'));
		let include5 = (num.toString().includes('5'));

		if ((divisibleBy3 && divisibleBy5)||(include3 && include5)){
			strArray.push(num + ' FizzBuzz');
		}
		else if (divisibleBy3 || include3){
			strArray.push(num + ' Fizz');
		} 
		else if (divisibleBy5 || include5){
			strArray.push(num + ' Buzz');
		}
		else {
			strArray.push(num + ' N/A');
		}
	})	
	
	return	strArray;
}

module.exports = ArrayVerify;